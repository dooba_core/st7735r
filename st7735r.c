/* Dooba SDK
 * ST7735R TFT LCD Display Driver
 */

// External Includes
#include <string.h>
#include <util/delay.h>
#include <util/bswap.h>
#include <util/math.h>
#include <spi/spi.h>

// Internal Includes
#include "st7735r.h"

// Pre-Initialize
void st7735r_preinit(uint8_t rst_pin, uint8_t dc_pin, uint8_t cs_pin)
{
	// Init I/O
	dio_output(rst_pin);
	dio_output(dc_pin);
	dio_output(cs_pin);
	dio_hi(rst_pin);
	dio_lo(dc_pin);
	dio_hi(cs_pin);
}

// Initialize
void st7735r_init(struct st7735r *dsp, uint8_t rst_pin, uint8_t dc_pin, uint8_t cs_pin, uint8_t pixels_x, uint8_t pixels_y, uint8_t pixels_xs, uint8_t pixels_ys)
{
	// Pre-Init
	st7735r_preinit(rst_pin, dc_pin, cs_pin);

	// Setup Structure
	dsp->rst_pin = rst_pin;
	dsp->dc_pin = dc_pin;
	dsp->cs_pin = cs_pin;
	dsp->pix_x = pixels_x;
	dsp->pix_y = pixels_y;
	dsp->pix_xs = pixels_xs;
	dsp->pix_ys = pixels_ys;
	dsp->cols = pixels_x;
	dsp->rows = pixels_y;
	dsp->col_s = pixels_xs;
	dsp->row_s = pixels_ys;
	dsp->dd_x = 0;
	dsp->dd_y = 0;
	dsp->dd_w = 0;
	dsp->dd_h = 0;

	// Enable Module
	_delay_ms(ST7735R_DELAY_INIT);
	dio_lo(dsp->rst_pin);
	_delay_ms(ST7735R_DELAY_INIT);
	dio_hi(dsp->rst_pin);
	_delay_ms(ST7735R_DELAY_INIT);

	// Trigger Software Reset
	st7735r_select(dsp);
	st7735r_cmd(dsp, ST7735R_SWRESET, 0);
	_delay_ms(ST7735R_DELAY_SWRESET);

	// Wake Display
	st7735r_cmd(dsp, ST7735R_SLPOUT, 0);

	// Delay
	_delay_ms(ST7735R_DELAY_SLPOUT);
	st7735r_deselect(dsp);
	_delay_ms(ST7735R_DELAY_SWRESET);

	// Configure Controller
	st7735r_select(dsp);
	st7735r_cmd(dsp, ST7735R_FRMCTR1, 3, 0x01, 0x2c, 0x2c);
	st7735r_cmd(dsp, ST7735R_FRMCTR2, 3, 0x01, 0x2c, 0x2c);
	st7735r_cmd(dsp, ST7735R_FRMCTR3, 6, 0x01, 0x2c, 0x2c, 0x01, 0x2c, 0x2c);
	st7735r_cmd(dsp, ST7735R_INVCTR, 1, 0x07);
	st7735r_cmd(dsp, ST7735R_PWCTR1, 3, 0xa2, 0x02, 0x84);
	st7735r_cmd(dsp, ST7735R_PWCTR2, 1, 0xc5);
	st7735r_cmd(dsp, ST7735R_PWCTR3, 2, 0x0a, 0x00);
	st7735r_cmd(dsp, ST7735R_PWCTR4, 2, 0x8a, 0x2a);
	st7735r_cmd(dsp, ST7735R_PWCTR5, 2, 0x8a, 0xee);
	st7735r_cmd(dsp, ST7735R_VMCTR1, 1, 0x0e);
	st7735r_cmd(dsp, ST7735R_INVOFF, 0);
	st7735r_cmd(dsp, ST7735R_MADCTL, 1, 0xc0);
	st7735r_cmd(dsp, ST7735R_COLMOD, 1, 0x05);
	st7735r_cmd(dsp, ST7735R_CASET, 4, 0x00, dsp->col_s, 0x00, (dsp->cols - 1) + dsp->col_s);
	st7735r_cmd(dsp, ST7735R_RASET, 4, 0x00, dsp->row_s, 0x00, (dsp->rows - 1) + dsp->row_s);
	st7735r_cmd(dsp, ST7735R_GMCTRP1, 16, 0x02, 0x1c, 0x07, 0x12, 0x37, 0x32, 0x29, 0x2d, 0x29, 0x25, 0x2B, 0x39, 0x00, 0x01, 0x03, 0x10);
	st7735r_cmd(dsp, ST7735R_GMCTRN1, 16, 0x03, 0x1d, 0x07, 0x06, 0x2E, 0x2C, 0x29, 0x2D, 0x2E, 0x2E, 0x37, 0x3F, 0x00, 0x00, 0x02, 0x10);
	st7735r_cmd(dsp, ST7735R_NORON, 0);
	_delay_ms(ST7735R_DELAY_NORON);
	st7735r_cmd(dsp, ST7735R_DISPON, 0);
	_delay_ms(ST7735R_DELAY_DISPON);
	st7735r_deselect(dsp);

	// Clear buffer
	memset(dsp->fb, 0, ST7735R_FB_SIZE * sizeof(uint16_t));
}

// Initialize Graphics Driver
void st7735r_init_gfx(struct gfx_dsp *gdsp, struct st7735r *dsp)
{
	// Init Graphics Driver
	gfx_dsp_init(gdsp, dsp);
	gfx_dsp_set_max_inv_size(gdsp, ST7735R_FB_SIZE);
	gdsp->set_orientation = (gfx_dsp_set_orientation_t)st7735r_set_orientation;
	gdsp->get_width = (gfx_dsp_get_width_t)st7735r_get_width;
	gdsp->get_height = (gfx_dsp_get_height_t)st7735r_get_height;
	gdsp->dsp_on = (gfx_dsp_on_t)st7735r_dsp_on;
	gdsp->dsp_off = (gfx_dsp_off_t)st7735r_dsp_off;
	gdsp->plot = (gfx_dsp_plot_t)st7735r_plot;
	gdsp->clr = (gfx_dsp_clr_t)st7735r_clr;
	gdsp->commit = (gfx_dsp_commit_t)st7735r_commit;
}

// Set Orientation
void st7735r_set_orientation(struct gfx_dsp *gdsp, struct st7735r *dsp, uint8_t orientation)
{
	uint8_t mctl;

	// Set Orientation Parameters
	switch(orientation)
	{
		// 0 - TOP
		case GFX_DSP_ORIENTATION_TOP:
			dsp->cols = dsp->pix_x;
			dsp->rows = dsp->pix_y;
			dsp->col_s = dsp->pix_xs;
			dsp->row_s = dsp->pix_ys;
			mctl = 0xc0;
			break;

		// 1 - BOTTOM
		case GFX_DSP_ORIENTATION_BOTTOM:
			dsp->cols = dsp->pix_x;
			dsp->rows = dsp->pix_y;
			dsp->col_s = dsp->pix_xs;
			dsp->row_s = dsp->pix_ys;
			mctl = 0x00;
			break;

		// 2 - LEFT
		case GFX_DSP_ORIENTATION_LEFT:
			dsp->cols = dsp->pix_y;
			dsp->rows = dsp->pix_x;
			dsp->col_s = dsp->pix_ys;
			dsp->row_s = dsp->pix_xs;
			mctl = 0x60;
			break;

		// 2 - RIGHT
		case GFX_DSP_ORIENTATION_RIGHT:
			dsp->cols = dsp->pix_y;
			dsp->rows = dsp->pix_x;
			dsp->col_s = dsp->pix_ys;
			dsp->row_s = dsp->pix_xs;
			mctl = 0xa0;
			break;

		default:
			return;
			break;
	}

	// Apply Orientation to Display Memory Access
	st7735r_select(dsp);
	st7735r_cmd(dsp, ST7735R_MADCTL, 1, mctl);
	st7735r_deselect(dsp);
}

// Get Display Width
uint16_t st7735r_get_width(struct gfx_dsp *gdsp, struct st7735r *dsp)
{
	// Width
	return dsp->cols;
}

// Get Display Height
uint16_t st7735r_get_height(struct gfx_dsp *gdsp, struct st7735r *dsp)
{
	// Height
	return dsp->rows;
}

// Display On
void st7735r_dsp_on(struct gfx_dsp *gdsp, struct st7735r *dsp)
{
	// Display On
	st7735r_select(dsp);
	st7735r_cmd(dsp, ST7735R_SLPOUT, 0);
	_delay_ms(ST7735R_DELAY_SLPOUT);
	st7735r_deselect(dsp);
}

// Display Off
void st7735r_dsp_off(struct gfx_dsp *gdsp, struct st7735r *dsp)
{
	// Display Off
	st7735r_select(dsp);
	st7735r_cmd(dsp, ST7735R_SLPIN, 0);
	_delay_ms(ST7735R_DELAY_SLPIN);
	st7735r_deselect(dsp);
}

// Set Draw Window
void st7735r_set_window(struct st7735r *dsp, uint8_t x, uint8_t y, uint8_t w, uint8_t h)
{
	// Adjust Position
	x = x + dsp->col_s;
	y = y + dsp->row_s;

	// Set Column & Row Coordinates
	st7735r_cmd(dsp, ST7735R_CASET, 4, 0, x, 0, x + w - 1);
	st7735r_cmd(dsp, ST7735R_RASET, 4, 0, y, 0, y + h - 1);

	// Write to RAM
	st7735r_cmd(dsp, ST7735R_RAMWR, 0);
}

// Plot
void st7735r_plot(struct gfx_dsp *gdsp, struct st7735r *dsp, uint16_t x, uint16_t y, uint16_t c)
{
	// Clip to Display
	if((x >= dsp->cols) || (y >= dsp->rows))				{ return; }

	// Check Direct-Drawing
	if(gdsp->direct_draw)
	{
		// Check Inval Region
		if((gdsp->inv_w > 0) || (gdsp->inv_h > 0))
		{
			// A buffer is already defined - do we fit in?
			if((x < gdsp->inv_x) || (x >= (gdsp->inv_x + gdsp->inv_w)) || (y < gdsp->inv_y) || (y >= (gdsp->inv_y + gdsp->inv_h)))
			{
				// Pixel is outside - commit buffer first (but only Direct Draw active part)
				st7735r_commit(gdsp, dsp);
			}
		}

		// Do we need a new buffer?
		if((gdsp->inv_w == 0) || (gdsp->inv_h == 0))
		{
			// No buffer defined yet - let's start one (centered around pixel)
			gdsp->inv_x = (x > (ST7735R_DD_W / 2)) ? (x - (ST7735R_DD_W / 2)) : 0;
			if((gdsp->inv_x + ST7735R_DD_W) > dsp->cols)	{ gdsp->inv_x = dsp->cols - ST7735R_DD_W; }
			gdsp->inv_y = (y > (ST7735R_DD_H / 2)) ? (y - (ST7735R_DD_H / 2)) : 0;
			if((gdsp->inv_y + ST7735R_DD_H) > dsp->rows)	{ gdsp->inv_y = dsp->rows - ST7735R_DD_H; }
			gdsp->inv_w = ST7735R_DD_W;
			gdsp->inv_h = ST7735R_DD_H;

			// Reset Direct Draw Stats
			dsp->dd_x = x;
			dsp->dd_y = y;
			dsp->dd_w = 1;
			dsp->dd_h = 1;
		}

		// Update Direct Draw Stats
		if(x < dsp->dd_x)									{ dsp->dd_x = x; }
		else if(x >= (dsp->dd_x + dsp->dd_w))				{ dsp->dd_w = (x + 1) - dsp->dd_x; }
		else												{ /* NoOp */ }
		if(y < dsp->dd_y)									{ dsp->dd_y = y; }
		else if(y >= (dsp->dd_y + dsp->dd_h))				{ dsp->dd_h = (y + 1) - dsp->dd_y; }
		else												{ /* NoOp */ }
	}

	// Plot in Framebuffer
	dsp->fb[((y - gdsp->inv_y) * gdsp->inv_w) + (x - gdsp->inv_x)] = bswap_s(c);
}

// Clear Area
void st7735r_clr(struct gfx_dsp *gdsp, struct st7735r *dsp, uint16_t x, uint16_t y, uint16_t w, uint16_t h)
{
	uint16_t i;

	// Check Direct-Drawing
	if(gdsp->direct_draw)
	{
		// Commit first
		st7735r_commit(gdsp, dsp);

		// Immediately draw
		st7735r_select(dsp);
		st7735r_set_window(dsp, x, y, w, h);
		spi_tx_s(0, w * h * 2);
		st7735r_deselect(dsp);
		return;
	}

	// Clear Framebuffer
	for(i = 0; i < h; i = i + 1)							{ memset(&(dsp->fb[(((y + i) - gdsp->inv_y) * gdsp->inv_w) + (x - gdsp->inv_x)]), 0, w * sizeof(uint16_t)); }
}

// Commit
void st7735r_commit(struct gfx_dsp *gdsp, struct st7735r *dsp)
{
	uint16_t i;

	// Select
	st7735r_select(dsp);

	// Check Draw Type
	if(gdsp->direct_draw)
	{
		/* Direct Draw
		 * Only commit "DD" window
		 */

		// Check Window
		if((dsp->dd_w) && (dsp->dd_h))
		{
			// Set Draw Window
			st7735r_set_window(dsp, dsp->dd_x, dsp->dd_y, dsp->dd_w, dsp->dd_h);

			// Update Direct Draw Stats
			dsp->dd_x = dsp->dd_x - gdsp->inv_x;
			dsp->dd_y = dsp->dd_y - gdsp->inv_y;

			// Draw
			for(i = 0; i < dsp->dd_h; i = i + 1)			{ spi_tx(&(dsp->fb[((dsp->dd_y + i) * gdsp->inv_w) + dsp->dd_x]), dsp->dd_w * 2); }

			// Clear
			for(i = 0; i < dsp->dd_h; i = i + 1)			{ memset(&(dsp->fb[((dsp->dd_y + i) * gdsp->inv_w) + dsp->dd_x]), 0, dsp->dd_w * 2); }
		}

		// Clear Invalidated Area
		gdsp->inv_w = 0;
		gdsp->inv_h = 0;
		dsp->dd_w = 0;
		dsp->dd_h = 0;
	}
	else
	{
		/* Normal Draw
		 * Commit entire invalidated area
		 */

		// Check Window
		if((gdsp->inv_w) && (gdsp->inv_h))
		{
			// Set Draw Window
			st7735r_set_window(dsp, gdsp->inv_x, gdsp->inv_y, gdsp->inv_w, gdsp->inv_h);

			// Commit partial framebuffer to invalidated region
			spi_tx(dsp->fb, gdsp->inv_w * gdsp->inv_h * 2);
		}
	}

	// Deselect
	st7735r_deselect(dsp);
}

// Send Command
void st7735r_cmd(struct st7735r *dsp, uint8_t cmd, uint8_t args, ...)
{
	va_list ap;
	uint8_t i;
	uint8_t x;

	// Issue Command
	dio_lo(dsp->dc_pin);
	spi_io(cmd);
	dio_hi(dsp->dc_pin);

	// Acquire & Process Args
	va_start(ap, args);
	for(i = 0; i < args; i = i + 1)
	{
		// Fetch Arg
		x = (uint8_t)va_arg(ap, uint16_t);

		// Send
		spi_io(x);
	}

	// Release Args
	va_end(ap);
}
