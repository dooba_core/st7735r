/* Dooba SDK
 * ST7735R TFT LCD Display Driver
 */

#ifndef	__ST7735R_H
#define	__ST7735R_H

// External Includes
#include <stdint.h>
#include <stdarg.h>
#include <avr/io.h>
#include <dio/dio.h>
#include <gfx/gfx.h>
#include <gfx/dsp.h>

/*
 *		Display Orientation
 *     _____________________
 *    /                     \
 *    |  _________________  |
 *    | |                 | |
 *    | |        ^        | |
 *    | |        0        | |
 *    | |                 | |
 *    | |                 | |
 *    | |                 | |
 *    | | < 2         3 > | |
 *    | |                 | |
 *    | |                 | |
 *    | |                 | |
 *    | |        1        | |
 *    | |        v        | |
 *    | |_________________| |
 *    |                     |
 *    | o o o o o o o o o o |
 *    \_____________________/
 *
 */

// Partial Framebuffer Size (pixels) - should be at least equal to the largest side of the display
#ifndef	ST7735R_FB_SIZE
#define	ST7735R_FB_SIZE									1024
#endif

// Direct Draw Buffer Width - THIS MUST NOT EXCEED ST7735R_FB_SIZE!
#define	ST7735R_DD_W									64
#define	ST7735R_DD_H									(ST7735R_FB_SIZE / ST7735R_DD_W)

// Basic Control Commands & Values
#define	ST7735R_NOP										0x00
#define	ST7735R_SWRESET									0x01
#define	ST7735R_RDDID									0x04
#define	ST7735R_RDDST									0x09
#define	ST7735R_SLPIN									0x10
#define	ST7735R_SLPOUT									0x11
#define	ST7735R_PTLON									0x12
#define	ST7735R_NORON									0x13
#define	ST7735R_INVOFF									0x20
#define	ST7735R_INVON									0x21
#define	ST7735R_DISPOFF									0x28
#define	ST7735R_DISPON									0x29
#define	ST7735R_CASET									0x2a
#define	ST7735R_RASET									0x2b
#define	ST7735R_RAMWR									0x2c
#define	ST7735R_RAMRD									0x2e
#define	ST7735R_PTLAR									0x30
#define	ST7735R_COLMOD									0x3a
#define	ST7735R_MADCTL									0x36
#define	ST7735R_RDID1									0xda
#define	ST7735R_RDID2									0xdb
#define	ST7735R_RDID3									0xdc
#define	ST7735R_RDID4									0xdd
#define ST7735R_FRMCTR1									0xb1
#define ST7735R_FRMCTR2									0xb2
#define ST7735R_FRMCTR3									0xb3
#define ST7735R_INVCTR									0xb4
#define ST7735R_DISSET5									0xb6
#define ST7735R_PWCTR1									0xc0
#define ST7735R_PWCTR2									0xc1
#define ST7735R_PWCTR3									0xc2
#define ST7735R_PWCTR4									0xc3
#define ST7735R_PWCTR5									0xc4
#define ST7735R_VMCTR1									0xc5
#define ST7735R_PWCTR6									0xfC
#define ST7735R_GMCTRP1									0xe0
#define ST7735R_GMCTRN1									0xe1

// Delays
#define	ST7735R_DELAY_INIT								100
#define	ST7735R_DELAY_SWRESET							150
#define	ST7735R_DELAY_SLPOUT							200
#define	ST7735R_DELAY_SLPIN								200
#define	ST7735R_DELAY_NORON								50
#define	ST7735R_DELAY_DISPON							100

// Chip Select Shortcuts
#define	st7735r_select(dsp)								dio_lo((dsp)->cs_pin)
#define	st7735r_deselect(dsp)							dio_hi((dsp)->cs_pin)

// ST7735R Display Structure
struct st7735r
{
	// Partial Framebuffer (for invalidated region)
	uint16_t fb[ST7735R_FB_SIZE];

	// Pins
	uint8_t rst_pin;
	uint8_t dc_pin;
	uint8_t cs_pin;

	// Actual Pixels
	uint8_t pix_x;
	uint8_t pix_y;
	uint8_t pix_xs;
	uint8_t pix_ys;

	// Column / Row Configuration (after orientation)
	uint16_t cols;
	uint16_t rows;
	uint8_t col_s;
	uint8_t row_s;

	// Direct Draw Stats
	uint16_t dd_x;
	uint16_t dd_y;
	uint16_t dd_w;
	uint16_t dd_h;
};

// Initialize
extern void st7735r_init(struct st7735r *dsp, uint8_t rst_pin, uint8_t dc_pin, uint8_t cs_pin, uint8_t pixels_x, uint8_t pixels_y, uint8_t pixels_xs, uint8_t pixels_ys);

// Pre-Initialize
extern void st7735r_preinit(uint8_t rst_pin, uint8_t dc_pin, uint8_t cs_pin);

// Initialize Graphics Driver
extern void st7735r_init_gfx(struct gfx_dsp *gdsp, struct st7735r *dsp);

// Set Orientation
extern void st7735r_set_orientation(struct gfx_dsp *gdsp, struct st7735r *dsp, uint8_t orientation);

// Get Display Width
extern uint16_t st7735r_get_width(struct gfx_dsp *gdsp, struct st7735r *dsp);

// Get Display Height
extern uint16_t st7735r_get_height(struct gfx_dsp *gdsp, struct st7735r *dsp);

// Display On
extern void st7735r_dsp_on(struct gfx_dsp *gdsp, struct st7735r *dsp);

// Display Off
extern void st7735r_dsp_off(struct gfx_dsp *gdsp, struct st7735r *dsp);

// Set Draw Window
extern void st7735r_set_window(struct st7735r *dsp, uint8_t x, uint8_t y, uint8_t w, uint8_t h);

// Plot
extern void st7735r_plot(struct gfx_dsp *gdsp, struct st7735r *dsp, uint16_t x, uint16_t y, uint16_t c);

// Clear Area
extern void st7735r_clr(struct gfx_dsp *gdsp, struct st7735r *dsp, uint16_t x, uint16_t y, uint16_t w, uint16_t h);

// Commit
extern void st7735r_commit(struct gfx_dsp *gdsp, struct st7735r *dsp);

// Send Command
extern void st7735r_cmd(struct st7735r *dsp, uint8_t cmd, uint8_t args, ...);

#endif
